<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Lugar;

class LugarController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {
        return Lugar::get();
    }

    public function show(String $lugarId) {
        return Lugar::findOrFail($lugarId);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
            'explicacion' => 'required|string',
            'museoId' => 'required|exists:museos,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $lugar = new Lugar;
        $lugar->nombre = $request->nombre;
        $lugar->descripcion = $request->descripcion;
        $lugar->explicacion = $request->explicacion;
        $lugar->museoId = $request->museoId;
        $lugar->save();
        return $lugar;
    }

    public function update(String $lugarId, Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
            'explicacion' => 'required|string',
            'museoId' => 'required|exists:museos,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $lugar = Lugar::findOrFail($lugarId);
        $lugar->nombre = $request->nombre;
        $lugar->descripcion = $request->descripcion;
        $lugar->explicacion = $request->explicacion;
        $lugar->museoId = $request->museoId;
        $lugar->save();
        return $lugar;
    }

    public function destroy(String $lugarId) {
        $lugar = Lugar::findOrFail($lugarId);
        $lugar->delete();
        return $lugar;
    }
}
