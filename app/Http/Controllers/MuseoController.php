<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Museo;

class MuseoController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {
        return Museo::get();
    }

    public function show(String $museoId) {
        return Museo::with('recorridos')->findOrFail($museoId);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'descipcion' => 'required|string',
            'imagen' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $museo = new Museo;
        $museo->nombre = $request->nombre;
        $museo->descipcion = $request->descipcion;
        $museo->imagen = $request->imagen;
        $museo->save();
        return $museo;
    }

    public function update(String $museoId, Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'descipcion' => 'required|string',
            'imagen' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $museo = Museo::findOrFail($museoId);
        $museo->nombre = $request->nombre;
        $museo->save();
        return $museo;

    }

    public function destroy(String $museoId) {
        $museo = Museo::findOrFail($museoId);
        $museo->delete();
        return $museo;
    }
}
