<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Recorrido;

class RecorridoController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {
        return Recorrido::get();
    }

    public function recorridosDeUnMuseo(String $museoId) {
        return Recorrido::where('museoId',$museoId)->without('museo')->get();
    }

    public function show(String $recorridoId) {
        return Recorrido::with('detalles.lugar')->findOrFail($recorridoId);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
            'horaInicio' => 'required|date',
            'horaFin' => 'required|date|after:startDate',
            'museoId' => 'required|exists:museos,id',

            'detalle' => 'required|array',
            'detalle.*.lugarId' => 'required|exists:lugares,id',
        ]);
        
        if ($validator->fails()) {
            return $validator->errors();
        }

        $recorrido = new Recorrido;
        $recorrido->nombre = $request->nombre;
        $recorrido->descripcion = $request->descripcion;
        $recorrido->horaInicio = Carbon::createFromFormat('d-m-Y H:i',$request->horaInicio);
        $recorrido->horaFin = Carbon::createFromFormat('d-m-Y H:i',$request->horaFin);
        $recorrido->museoId = $request->museoId;
        $recorrido->save();

        $total = count($request->detalle);
        foreach ($request->detalle as $i => $detalle) {
            $detalle['recorridoId'] = $recorrido->id;
            $detalle['parte'] = $i+1;
            $detalle['total'] = $total;
            DetalleController::store(new Request($detalle));
        }

        return $recorrido;
    }

    public function update(String $recorridoId, Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
            'horaInicio' => 'required|date',
            'horaFin' => 'required|date|after:startDate',
            'museoId' => 'required|exists:museos,id',

            'detalle' => 'required|array',
            'detalle.*.id' => 'required|exists:detalles,id',
            'detalle.*.lugarId' => 'required|exists:lugares,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $recorrido = Recorrido::findOrFail($recorridoId);
        $recorrido->nombre = $request->nombre;
        $recorrido->descripcion = $request->descripcion;
        $recorrido->horaInicio = Carbon::createFromFormat('d-m-Y H:i',$request->horaInicio);
        $recorrido->horaFin = Carbon::createFromFormat('d-m-Y H:i',$request->horaFin);
        $recorrido->museoId = $request->museoId;
        $recorrido->save();

        $total = count($request->detalle);
        foreach ($request->detalle as $i => $detalle) {
            $detalle['recorridoId'] = $recorrido->id;
            $detalle['parte'] = $i+1;
            $detalle['total'] = $total;
            DetalleController::update($detalle['id'],new Request($detalle));
        }

        return $recorrido;
    }

    public function destroy(String $recorridoId) {
        $recorrido = Recorrido::findOrFail($recorridoId);
        $recorrido->delete();
        return $recorrido;
    }
}
