<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Detalle;

class DetalleController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {
        return Detalle::get();
    }

    public function show(String $detalleId) {
        return Detalle::findOrFail($detalleId);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'parte' => 'required|integer',
            'total' => 'required|integer',
            'recorridoId' => 'required|exists:recorridos,id',
            'lugarId' => 'required|exists:lugares,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $detalle = new Detalle;
        $detalle->parte = $request->parte;
        $detalle->total = $request->total;
        $detalle->recorridoId = $request->recorridoId;
        $detalle->lugarId = $request->lugarId;
        $detalle->save();
        
        return $detalle;
    }

    public function update(String $detalleId,Request $request) {
        $validator = Validator::make($request->all(), [
            'parte' => 'required|integer',
            'total' => 'required|integer',
            'recorridoId' => 'required|exists:recorridos,id',
            'lugarId' => 'required|exists:lugares,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $detalle = Detalle::findOrFail($detalleId);
        $detalle->parte = $request->parte;
        $detalle->total = $request->total;
        $detalle->recorridoId = $request->recorridoId;
        $detalle->lugarId = $request->lugarId;
        $detalle->save();

        return $detalle;
    }

    public function destroy(String $detalleId) {
        $detalle = Detalle::findOrFail($detalleId);
        $detalle->delete();
        return $detalle;
    }
}
