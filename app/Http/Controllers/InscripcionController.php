<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Inscripcion;

class InscripcionController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {
        return Inscripcion::get();
    }

    public function misInscripciones(String $dni) {
        return Inscripcion::where('dni',$dni)->get();
    }

    public function show(String $inscripcionId) {
        return Inscripcion::findOrFail($inscripcionId);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'dni' => 'required|string',
            'recorridoId' => 'required|exists:recorridos,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $inscripcion = new Inscripcion;
        $inscripcion->dni = $request->dni;
        $inscripcion->recorridoId = $request->recorridoId;
        $inscripcion->save();
        return $inscripcion;
    }

    public function update(String $inscripcionId, Request $request) {
        $validator = Validator::make($request->all(), [
            'mac' => 'required|string',
            'recorridoId' => 'required|exists:recorridos,id',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $inscripcion = Inscripcion::findOrFail($inscripcionId);
        $inscripcion->mac = $request->mac;
        $inscripcion->recorridoId = $request->recorridoId;
        $inscripcion->save();
        return $inscripcion;
    }

    public function destroy(String $inscripcionId) {
        $inscripcion = Inscripcion::findOrFail($inscripcionId);
        $inscripcion->delete();
        return $inscripcion;
    }
}
