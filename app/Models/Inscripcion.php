<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    protected $table= "inscripciones";
    protected $with = ['recorrido'];

    public function recorrido(){
        return $this->belongsTo(Recorrido::class, 'recorridoId', 'id');
    }
}