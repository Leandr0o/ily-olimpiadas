<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Museo extends Model
{
    protected $table= "museos";

    public function recorridos() {
        return $this->HasMany(Recorrido::class, 'museoId', 'id');
    }
    public function lugares() {
        return $this->HasMany(Lugar::class, 'museoId', 'id');
    }
}