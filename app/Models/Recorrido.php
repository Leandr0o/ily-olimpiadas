<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recorrido extends Model
{
    protected $table= "recorridos";
    protected $with = ['museo'];

    public function museo(){
        return $this->belongsTo(Museo::class, 'museoId', 'id');
    }

    public function detalles() {
        return $this->HasMany(Detalle::class, 'recorridoId', 'id');
    }

    public function inscripciones() {
        return $this->HasMany(Recorrido::class, 'recorridoId', 'id');
    }
}