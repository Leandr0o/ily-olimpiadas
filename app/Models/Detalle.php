<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    protected $table= "detalles";

    public function lugar(){
        return $this->belongsTo(Lugar::class, 'lugarId', 'id');
    }

    public function recorrido() {
        return $this->belongsTo(Recorrido::class, 'recorridoId', 'id');
    }
}