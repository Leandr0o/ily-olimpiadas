<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api'], function () use ($router) {
    $router->group(['prefix' => 'museos'], function () use ($router) {
        $router->get('/', 'MuseoController@index');
        $router->get('{museoId}', 'MuseoController@show');
        $router->post('/', 'MuseoController@store');
        $router->put('{museoId}', 'MuseoController@update');
        $router->delete('{museoId}', 'MuseoController@destroy');
        $router->get('{museoId}/recorridos', 'RecorridoController@recorridosDeUnMuseo');
    });

    $router->group(['prefix' => 'recorridos'], function () use ($router) {
        $router->get('/', 'RecorridoController@index');
        $router->get('{recorridoId}', 'RecorridoController@show');
        $router->post('/', 'RecorridoController@store');
        $router->put('{recorridoId}', 'RecorridoController@update');
        $router->delete('{recorridoId}', 'RecorridoController@destroy');
    });

    $router->group(['prefix' => 'lugares'], function () use ($router) {
        $router->get('/', 'LugarController@index');
        $router->get('{lugarId}', 'LugarController@show');
        $router->post('/', 'LugarController@store');
        $router->put('{lugarId}', 'LugarController@update');
        $router->delete('{lugarId}', 'LugarController@destroy');
    });

    $router->group(['prefix' => 'inscripciones'], function () use ($router) {
        $router->get('/', 'InscripcionController@index');
        $router->get('dni/{dni}', 'InscripcionController@misInscripciones');
        $router->get('{inscripcionId}', 'InscripcionController@show');
        $router->post('/', 'InscripcionController@store');
        $router->put('{inscripcionId}', 'InscripcionController@update');
        $router->delete('{inscripcionId}', 'InscripcionController@destroy');
    });

    $router->group(['prefix' => 'detalles'], function () use ($router) {
        $router->get('/', 'DetalleController@index');
        $router->get('{detalleId}', 'DetalleController@show');
        $router->post('/', 'DetalleController@store');
        $router->put('{detalleId}', 'DetalleController@update');
        $router->delete('{detalleId}', 'DetalleController@destroy');
    });
});